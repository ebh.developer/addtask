import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';
import 'package:task_list/data/data.dart';
import 'package:task_list/data/repo/repository.dart';
import 'package:task_list/data/source/hive_task_source.dart';
import 'package:task_list/screens/home/home.dart';

const taskBoxName = 'tasks';
void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(TaskAdapter());
  Hive.registerAdapter(PriorityAdapter());
  await Hive.openBox<TaskEntity>(taskBoxName);
  SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: primaryContainer));

  runApp(ChangeNotifierProvider<Repository<TaskEntity>>(
      create: (context) =>
          Repository<TaskEntity>(HiveTaskDataSource(Hive.box(taskBoxName))),
      child: const MyApp()));
}

const Color primaryColor = Color(0xff794cff);
const Color primaryContainer = Color(0xff5c0aff);
const secondaryTextColor = Color(0xffafbed0);
const normalPriority = Color(0xffF09819);
const lowPriority = Color(0xff3EB1F1);
const highPriority = primaryContainer;

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final primaryTextColor = Color(0xff1d2830);

    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          textTheme: GoogleFonts.poppinsTextTheme(const TextTheme(
              titleLarge: TextStyle(fontWeight: FontWeight.bold))),
          inputDecorationTheme: const InputDecorationTheme(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              border: InputBorder.none,
              labelStyle: TextStyle(
                color: secondaryTextColor,
              ),
              prefixIconColor: secondaryTextColor),
          colorScheme: ColorScheme.light(
              primaryContainer: primaryContainer,
              primary: primaryColor,
              background: const Color(0xfff3f5f8),
              onSurface: primaryTextColor,
              onBackground: primaryTextColor),
          useMaterial3: true,
        ),
        home: HomeScreen());
  }
}
