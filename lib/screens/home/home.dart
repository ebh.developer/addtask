import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:task_list/data/data.dart';
import 'package:task_list/data/repo/repository.dart';
import 'package:task_list/main.dart';
import 'package:task_list/screens/edit/cubit/edit_task_cubit.dart';
import 'package:task_list/screens/edit/edit.dart';
import 'package:task_list/screens/home/bloc/task_list_bloc.dart';
import 'package:task_list/widget.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});

  final TextEditingController _controller = TextEditingController();
  final ValueNotifier<String> searchKeyboardNotifier = ValueNotifier('');
  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => BlocProvider<EditTaskCubit>(
                    create: (context) => EditTaskCubit(
                        TaskEntity(), context.read<Repository<TaskEntity>>()),
                    child: const EditTaskScreen())));
          },
          label: const Row(
            children: [
              Icon(CupertinoIcons.add),
              SizedBox(
                width: 4,
              ),
              Text('Add New Task'),
            ],
          )),
      body: BlocProvider<TaskListBloc>(
        create: (context) =>
            TaskListBloc(context.read<Repository<TaskEntity>>()),
        child: SafeArea(
          child: Column(
            children: [
              Container(
                height: 110,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                  themeData.colorScheme.primary,
                  themeData.colorScheme.primaryContainer
                ])),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'To Do List',
                            style: themeData.textTheme.titleLarge!
                                .apply(color: themeData.colorScheme.onPrimary),
                          ),
                          Icon(CupertinoIcons.share,
                              color: themeData.colorScheme.onPrimary)
                        ],
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Container(
                        height: 38,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(19),
                            color: themeData.colorScheme.onPrimary,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black.withOpacity(0.1),
                                  blurRadius: 20)
                            ]),
                        child: Builder(
                          builder: (context) {
                            return TextField(
                              controller: _controller,
                              onChanged: (value) => {
                                context
                                    .read<TaskListBloc>()
                                    .add(TaskListSearch(value))
                              },
                              decoration: const InputDecoration(
                                  prefixIcon: Icon(CupertinoIcons.search),
                                  label: Text('Search Tasks...')),
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(child: Consumer<Repository<TaskEntity>>(
                builder: (context, model, child) {
                  context.read<TaskListBloc>().add(TaskListStarted());
                  return BlocBuilder<TaskListBloc, TaskListState>(
                      builder: (context, state) {
                    if (state is TaskListSuccess) {
                      return TaskList(items: state.items, themeData: themeData);
                    } else if (state is TaskListEmpty) {
                      return const EmptyState();
                    } else if (state is TaskListLoading ||
                        state is TaskListInitial) {
                      return const Center(child: CircularProgressIndicator());
                    } else if (state is TaskListError) {
                      return Center(
                        child: Text(state.errorMessage),
                      );
                    } else {
                      throw Exception('state is not valid..');
                    }
                  });
                },
              )),
            ],
          ),
        ),
      ),
    );
  }
}

class TaskList extends StatelessWidget {
  const TaskList({
    super.key,
    required this.items,
    required this.themeData,
  });

  final List<TaskEntity> items;
  final ThemeData themeData;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.fromLTRB(16, 16, 16, 100),
        itemCount: items.length + 1,
        itemBuilder: (context, index) {
          if (index == 0) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Today',
                      style: themeData.textTheme.titleLarge!
                          .apply(fontSizeFactor: 0.9),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 4),
                      height: 3,
                      width: 70,
                      decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(1.5)),
                    )
                  ],
                ),
                MaterialButton(
                    elevation: 0,
                    color: const Color(0xffEAEFF5),
                    textColor: secondaryTextColor,
                    onPressed: () {
                      context.read<TaskListBloc>().add(TaskListDeleteAll());
                    },
                    child: const Row(
                      children: [
                        Text('Delete All'),
                        SizedBox(
                          width: 4,
                        ),
                        Icon(
                          CupertinoIcons.delete_solid,
                          size: 18,
                        ),
                      ],
                    ))
              ],
            );
          } else {
            final TaskEntity tasks = items.toList()[index - 1];
            return _TaskItem(tasks: tasks);
          }
        });
  }
}

class _TaskItem extends StatefulWidget {
  static const double height = 74;
  static const double borderRadius = 8;
  const _TaskItem({
    super.key,
    required this.tasks,
  });

  final TaskEntity tasks;

  @override
  State<_TaskItem> createState() => _TaskItemState();
}

class _TaskItemState extends State<_TaskItem> {
  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    final Color priorityColor;

    switch (widget.tasks.priority) {
      case Priority.low:
        priorityColor = lowPriority;
        break;
      case Priority.normal:
        priorityColor = normalPriority;
        break;
      case Priority.high:
        priorityColor = highPriority;
        break;
    }

    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (contex) => BlocProvider<EditTaskCubit>(
                    create: (context) => EditTaskCubit(
                        widget.tasks, contex.read<Repository<TaskEntity>>()),
                    child: const EditTaskScreen())));
      },
      onLongPress: () {
        context
            .read<TaskListBloc>()
            .add(TaskListDelete(taskEntity: widget.tasks));
      },
      child: Container(
        margin: const EdgeInsets.only(top: _TaskItem.borderRadius),
        padding: const EdgeInsets.only(left: 16),
        height: _TaskItem.height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: themeData.colorScheme.surface,
        ),
        child: Row(
          children: [
            MyCheckBox(
              value: widget.tasks.isCompleted,
              onTab: () {
                setState(() {
                  widget.tasks.isCompleted = !widget.tasks.isCompleted;
                });
              },
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
              child: Text(
                widget.tasks.name,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    decoration: widget.tasks.isCompleted
                        ? TextDecoration.lineThrough
                        : null),
              ),
            ),
            const SizedBox(
              width: 8,
            ),
            Container(
              width: 5,
              height: _TaskItem.height,
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(_TaskItem.borderRadius),
                    bottomRight: Radius.circular(_TaskItem.borderRadius),
                  ),
                  color: priorityColor),
            )
          ],
        ),
      ),
    );
  }
}
