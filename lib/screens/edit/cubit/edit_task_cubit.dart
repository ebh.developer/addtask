import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:task_list/data/data.dart';
import 'package:task_list/data/repo/repository.dart';

part 'edit_task_state.dart';

class EditTaskCubit extends Cubit<EditTaskState> {
  final TaskEntity _tasks;
  final Repository<TaskEntity> repository;
  EditTaskCubit(this._tasks, this.repository)
      : super(EditTaskInitial(taskEntity: _tasks));

  void onSaveChangeClick() {
    repository.createOrUpdate(_tasks);
  }

  void onTextChanged(String text) {
    _tasks.name = text;
  }

  void onPriorityChanged(Priority priority) {
    _tasks.priority = priority;
    emit(EditTaskPriorityChange(taskEntity: _tasks));
  }
}
