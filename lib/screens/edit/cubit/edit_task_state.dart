part of 'edit_task_cubit.dart';

@immutable
sealed class EditTaskState {
  final TaskEntity taskEntity;

  const EditTaskState({required this.taskEntity});
}

final class EditTaskInitial extends EditTaskState {
  const EditTaskInitial({required super.taskEntity});
}

class EditTaskPriorityChange extends EditTaskState {
  const EditTaskPriorityChange({required super.taskEntity});
}
